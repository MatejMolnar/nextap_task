//
//  StoriesSwiperCellViewModeling.swift
//  nextap_task
//
//  Created by Matej Molnár on 17/10/2020.
//

import Foundation

protocol StorySwiperCellViewModeling {
    var id: String { get }
    var name: String { get }
    var imageUrl: URL? { get }
    var userImageUrl: URL? { get }
}
