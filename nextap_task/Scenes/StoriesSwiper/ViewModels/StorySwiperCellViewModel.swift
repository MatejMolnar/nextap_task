//
//  StorySwiperCellViewModel.swift
//  nextap_task
//
//  Created by Matej Molnár on 18/10/2020.
//

import Foundation

class StorySwiperCellViewModel: StorySwiperCellViewModeling {
    
    var id: String
    var name: String
    var imageUrl: URL?
    var userImageUrl: URL?
    
    init(_ model: Story) {
        id = model.id
        name = model.userName
        imageUrl = URL(string: model.coverImageUrl)
        userImageUrl = URL(string: model.userImageUrl)
    }
}
