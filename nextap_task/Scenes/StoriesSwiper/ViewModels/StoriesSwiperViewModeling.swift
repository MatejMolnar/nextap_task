//
//  StoriesSwiperViewModeling.swift
//  nextap_task
//
//  Created by Matej Molnár on 17/10/2020.
//

import RxSwift

protocol StoriesSwiperViewModeling {
    var storiesCellModelArray: [StorySwiperCellViewModeling] { get }
    var initialIndex: Int { get }
    var currentIndex: Int { get set }
    var closeSubject: PublishSubject<Void> { get }
}
