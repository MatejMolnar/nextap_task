//
//  StoriesSwiperViewModel.swift
//  nextap_task
//
//  Created by Matej Molnár on 17/10/2020.
//

import RxSwift

class StoriesSwiperViewModel: StoriesSwiperViewModeling {
            
    let storiesCellModelArray: [StorySwiperCellViewModeling]
    let initialIndex: Int
    var currentIndex: Int
    let closeSubject = PublishSubject<Void>()
    
    private let disposeBag = DisposeBag()

    init(storiesCellModelArray: [StorySwiperCellViewModeling], initialIndex: Int) {
        self.storiesCellModelArray = storiesCellModelArray
        self.initialIndex = initialIndex
        self.currentIndex = initialIndex
    }
}
