//
//  StorySwiperCell.swift
//  nextap_task
//
//  Created by Matej Molnár on 18/10/2020.
//

import UIKit
import FSPagerView

private let userImageSize: CGFloat = 50

class StorySwiperCell: FSPagerViewCell {
    
    var viewModel: StorySwiperCellViewModeling? {
        didSet {
            guard let viewModel = viewModel else { return }
            nameLabel.text = viewModel.name
            imageView?.kf.setImage(with: viewModel.imageUrl, options: [.transition(.fade(0.2))])
            userImageView.kf.setImage(with: viewModel.userImageUrl, options: [.transition(.fade(0.2))])
        }
    }
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15, weight: .medium)
        label.textColor = .white
        return label
    }()
    
    private lazy var userImageView: UIImageView = {
        let view = UIImageView()
        view.layer.cornerRadius = userImageSize/2
        view.layer.masksToBounds = true
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.white.cgColor
        view.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
        view.contentMode = .scaleAspectFill
        return view
    }()
    
    private let gradientView: GradientView = {
        let view = GradientView()
        view.startColor = UIColor.black.withAlphaComponent(0.9)
        view.endColor = UIColor.black.withAlphaComponent(0)
        return view
    }()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setup() {
        contentView.backgroundColor = .white
        
        imageView?.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
        imageView?.contentMode = .scaleAspectFill
        imageView?.layer.masksToBounds = true
        
        contentView.sv(
            gradientView,
            nameLabel,
            userImageView
        )
        
        gradientView
            .left(0).right(0)
            .top(0)
            .height(120)
        
        userImageView
            .left(10)
            .top(SafeAreaHeight.top + 10)
            .size(userImageSize)
        
        nameLabel
            .left(userImageView, .right, 15).right(60)
            .centerY(userImageView)
    }
}
