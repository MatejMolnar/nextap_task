//
//  StoriesSwiperViewController.swift
//  nextap_task
//
//  Created by Matej Molnár on 17/10/2020.
//

import RxSwift
import UIKit
import FSPagerView
import RxCocoa

private let storyCellIdentifier = "StoryCell"

class StoriesSwiperViewController: UIViewController {
    
    private var viewModel: StoriesSwiperViewModeling
    private let disposeBag = DisposeBag()
    private let closeButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "close"), for: .init())
        
        return button
    }()
        
    private lazy var pagerView: FSPagerView = {
        let view = FSPagerView()
        view.transformer = FSPagerViewTransformer(type: .cubic)
        view.register(StorySwiperCell.self, forCellWithReuseIdentifier: storyCellIdentifier)
        view.dataSource = self
        view.delegate = self
        return view
    }()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    init(viewModel: StoriesSwiperViewModeling) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        closeButton.rx.tap.bind(to: viewModel.closeSubject).disposed(by: disposeBag)
        
        setupUI()
                
        view.layoutIfNeeded()
        pagerView.reloadData()
        pagerView.scrollToItem(at: viewModel.initialIndex, animated: false)
    }
    
    private func setupUI() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapLeftGestureRecognizerHandler))
        pagerView.addGestureRecognizer(tap)
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognizerHandler(_:)))
        view.addGestureRecognizer(pan)
        
        view.sv(
            pagerView,
            closeButton)
        
        pagerView
            .pinToSuperview()
        
        closeButton
            .top(SafeAreaHeight.top + 10).right(10)
            .size(35)
    }
    
    // MARK: - GestureRecognizerHandlers
    @IBAction func tapLeftGestureRecognizerHandler(_ sender: UITapGestureRecognizer) {
        let tap = sender.location(in: pagerView)
        
        var index = pagerView.currentIndex
        
        if tap.x > pagerView.bounds.width/2 {
            index += 1
        } else {
            index -= 1
        }
                
        if index >= 0 {
            pagerView.scrollToItem(at: index, animated: true)
        }
    }
    
    @IBAction func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {

        var translationY = sender.translation(in: sender.view!).y
        translationY = translationY >= 0 ? translationY : 0
        
        switch sender.state {
        case .began: break
        case .changed: view.transform = CGAffineTransform(translationX: 0, y: translationY)
        case .ended, .cancelled:
            if translationY > 160 {
                viewModel.closeSubject.onNext(())
            } else {
                UIView.animate(withDuration: 0.2, animations: {
                    self.view.transform = CGAffineTransform(translationX: 0, y: 0)
                })
            }
        case .failed, .possible: break
        @unknown default: break
        }
    }
}

extension StoriesSwiperViewController: FSPagerViewDataSource {
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return viewModel.storiesCellModelArray.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        guard let cell = pagerView.dequeueReusableCell(withReuseIdentifier: storyCellIdentifier, at: index) as? StorySwiperCell else {
            return FSPagerViewCell()
        }
        
        cell.viewModel = viewModel.storiesCellModelArray.safeObject(index)
        
        return cell
    }
}

extension StoriesSwiperViewController: FSPagerViewDelegate {
    func pagerViewDidScroll(_ pagerView: FSPagerView) {
        viewModel.currentIndex = pagerView.currentIndex
    }
}
