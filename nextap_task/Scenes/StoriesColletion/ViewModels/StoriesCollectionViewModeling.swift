//
//  StoriesCollectionViewModeling.swift
//  nextap_task
//
//  Created by Matej Molnár on 17/10/2020.
//

import RxSwift
import RxRelay

protocol StoriesCollectionViewModeling {
    var navigationBarTitle: String { get }
    var storiesCellModelArrayRelay: BehaviorRelay<[StoryCollectionCellViewModeling]> { get }
    var didSelectStoryObservable: Observable<(stories: [Story], selectedIndex: Int)> { get }
    var newlySelectedStoryIndex: PublishRelay<Int> { get }
    
    func fetchStories()
    func selectCellAtIndex(_ index: Int)    
}
