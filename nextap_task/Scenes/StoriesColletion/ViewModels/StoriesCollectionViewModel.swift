//
//  StoriesCollectionViewModel.swift
//  nextap_task
//
//  Created by Matej Molnár on 17/10/2020.
//

import RxRelay
import RxSwift

class StoriesCollectionViewModel: StoriesCollectionViewModeling {
    
    private let storiesService: StoriesServicing
    private var fechStoriesDisposable: Disposable?
    private let didSelectStorySubject = PublishSubject<(stories: [Story], selectedIndex: Int)>()
    private var currentStories = [Story]() {
        didSet {
            let array = currentStories.map { StoryCollectionCellViewModel($0) }
            storiesCellModelArrayRelay.accept(array)
        }
    }
                
    let navigationBarTitle: String = "STELLER"
    let storiesCellModelArrayRelay = BehaviorRelay<[StoryCollectionCellViewModeling]>(value: [StoryCollectionCellViewModeling]())
    let newlySelectedStoryIndex = PublishRelay<Int>()
    var didSelectStoryObservable: Observable<(stories: [Story], selectedIndex: Int)> {
        return didSelectStorySubject.asObservable()
    }
    
    init(storiesService: StoriesServicing) {
        self.storiesService = storiesService
    }
    
    func fetchStories() {
        fechStoriesDisposable?.dispose()
        
        fechStoriesDisposable = storiesService.fetchStoriesSingle()
            .subscribe { [weak self] (stories) in
                self?.currentStories = stories
            }
    }
    
    func selectCellAtIndex(_ index: Int) {
        didSelectStorySubject.onNext((currentStories, index))
    }
}
