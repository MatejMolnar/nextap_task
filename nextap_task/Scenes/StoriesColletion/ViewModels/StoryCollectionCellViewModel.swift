//
//  StoryCollectionCellViewModel.swift
//  nextap_task
//
//  Created by Matej Molnár on 17/10/2020.
//

import Foundation

class StoryCollectionCellViewModel: StoryCollectionCellViewModeling {
    var id: String
    var name: String
    var imageUrl: URL?
    var userImageUrl: URL?
    
    init(_ model: Story) {
        id = model.id
        name = model.userName
        imageUrl = URL(string: model.coverImageUrl)
        userImageUrl = URL(string: model.userImageUrl)
    }
}
