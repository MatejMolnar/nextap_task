//
//  StoryCollectionCellView.swift
//  nextap_task
//
//  Created by Matej Molnár on 17/10/2020.
//

import UIKit
import Kingfisher

private let userImageSize: CGFloat = 50

class StoryCollectionCell: UICollectionViewCell {
    static let heroIdentifier = "story"
    
    var viewModel: StoryCollectionCellViewModeling? {
        didSet {
            guard let viewModel = viewModel else { return }
            
            nameLabel.text = viewModel.name
            imageView.kf.setImage(with: viewModel.imageUrl, options: [.transition(.fade(0.5))])
            userImageView.kf.setImage(with: viewModel.userImageUrl, options: [.transition(.fade(0.2))])
        }
    }
    
    let containerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 15
        view.layer.masksToBounds = true
        return view
    }()
    
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15, weight: .medium)
        label.textColor = UIColor.white
        return label
    }()
    
    lazy var imageView: UIImageView = {
        let view = UIImageView()
        view.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
        view.contentMode = .scaleAspectFill
        return view
    }()
    
    lazy var userImageView: UIImageView = {
        let view = UIImageView()
        view.layer.cornerRadius = userImageSize/2
        view.layer.masksToBounds = true
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.white.cgColor
        view.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
        view.contentMode = .scaleAspectFill
        return view
    }()
    
    private let gradientView: GradientView = {
        let view = GradientView()
        view.startColor = UIColor.black.withAlphaComponent(0)
        view.endColor = UIColor.black.withAlphaComponent(0.7)
        return view
    }()
    
    override var isHighlighted: Bool {
        didSet {
            UIView.animate(withDuration: 0.3) {
                self.transform = self.isHighlighted ? CGAffineTransform(scaleX: 0.96, y: 0.96) : CGAffineTransform.identity
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        contentView.sv(
            containerView.sv(
                imageView.sv(
                    gradientView
                ),
                nameLabel,
                userImageView
            )
        )
        
        containerView
            .pinToSuperview()
        
        imageView
            .pinToSuperview()
        
        gradientView
            .left(0).right(0)
            .bottom(0).height(80)
        
        nameLabel
            .left(10).right(0)
            .bottom(10)
        
        userImageView
            .left(10).top(10)
            .size(userImageSize)
    }
}
