//
//  StoriesCollectionViewController.swift
//  nextap_task
//
//  Created by Matej Molnár on 17/10/2020.
//

import UIKit
import RxSwift
import Hero

private let cellHeight: CGFloat = 150
private let cellHorizontalSpacing: CGFloat = 12
private let cellVerticalSpacing: CGFloat = 20
private let cellHorizontalEdgeInset: CGFloat = 4

class StoriesCollectionViewController: UIViewController {
    
    private let viewModel: StoriesCollectionViewModeling
    private let disposeBag = DisposeBag()
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = cellVerticalSpacing
        layout.minimumInteritemSpacing = cellHorizontalSpacing
        
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
                        
        collection.registerCellClass(ofType: StoryCollectionCell.self)
        collection.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        collection.contentInset = UIEdgeInsets(
            top: cellVerticalSpacing,
            left: cellHorizontalEdgeInset,
            bottom: cellVerticalSpacing,
            right: cellHorizontalEdgeInset)
        collection.backgroundColor = .clear
        collection.dataSource = self
        collection.delegate = self
        
        collection.layer.shadowRadius = 7
        collection.layer.shadowOpacity = 0.5
        collection.layer.shadowColor = UIColor.black.cgColor
        collection.layer.shadowOffset = CGSize(width: 5, height: 5)
        
        return collection
    }()
    
    private var selectedItemIndex: Int = 0 {
        didSet {            
            collectionView.scrollToItem(at: IndexPath(item: selectedItemIndex, section: 0), at: .top, animated: false)
            collectionView.reloadData()
        }
    }
    
    init(viewModel: StoriesCollectionViewModeling) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupRx()
                    
        viewModel.fetchStories()
    }
    
    private func setupRx() {
        viewModel
            .newlySelectedStoryIndex
            .observeOn(MainScheduler.instance)
            .bind { [weak self] index in
                self?.selectedItemIndex = index
            }
            .disposed(by: disposeBag)
        
        viewModel
            .storiesCellModelArrayRelay
            .observeOn(MainScheduler.instance)
            .do(afterNext: { [weak self] (_) in
                guard let self = self else { return }
                
                if !self.collectionView.visibleCells.isEmpty {
                    self.collectionView.reloadData()
                } else {
                    UIView.transition(
                        with: self.collectionView,
                        duration: 0.3,
                        options: .transitionCrossDissolve,
                        animations: {
                            self.collectionView.reloadData()
                        })
                }
            })
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    private func setupUI() {
        view.backgroundColor = .white
        title = viewModel.navigationBarTitle
        
        view.sv(collectionView)
        
        collectionView.pinToSuperview()
    }
    
    private func resetAllCellHeroIdentifiers() {
        for view in collectionView.subviews {
            if let storyCell = view as? StoryCollectionCell {
                storyCell.containerView.heroID = ""
            }
        }
    }
}

extension StoriesCollectionViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.storiesCellModelArrayRelay.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeReusableCell(ofType: StoryCollectionCell.self, for: indexPath)
        
        cell.viewModel = viewModel.storiesCellModelArrayRelay.value.safeObject(indexPath.item)
        
        cell.containerView.heroID = indexPath.item == selectedItemIndex ? StoryCollectionCell.heroIdentifier : ""
        
        return cell
    }
}

extension StoriesCollectionViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard case let cell as StoryCollectionCell = collectionView.cellForItem(at: indexPath) else { return }
        
        resetAllCellHeroIdentifiers()
        
        cell.containerView.heroID = StoryCollectionCell.heroIdentifier
        
        viewModel.selectCellAtIndex(indexPath.item)
    }
}

extension StoriesCollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.bounds.width/2 - cellHorizontalSpacing/2 - cellHorizontalEdgeInset
        
        return CGSize(width: width, height: width*2)
    }
}
