//
//  AppCoordinator.swift
//  nextap_task
//
//  Created by Matej Molnár on 20/10/2020.
//

import UIKit

class AppCoordinator: Coordinator {
    weak var navigationController: UINavigationController?
    
    var parentCoordinator: Coordinator?
    
    var childCoordinators = [Coordinator]()
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        coordinate(to: AppDelegate.container.resolve(StoriesCoordinator.self, argument: navigationController!)!)
    }
}
