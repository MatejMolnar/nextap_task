//
//  StoriesCoordinator.swift
//  nextap_task
//
//  Created by Matej Molnár on 17/10/2020.
//

import UIKit
import Hero
import RxSwift

class StoriesCoordinator: Coordinator {
        
    var parentCoordinator: Coordinator?
    var childCoordinators = [Coordinator]()
    
    weak var navigationController: UINavigationController?
    
    private let disposeBag = DisposeBag()
    private let storiesCollectionViewModel: StoriesCollectionViewModeling
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.storiesCollectionViewModel = AppDelegate.container.resolve(StoriesCollectionViewModeling.self)!
    }
    
    func start() {
        showStoriesCollection()
    }
    
    private func showStoriesCollection() {
        let vc = StoriesCollectionViewController(viewModel: storiesCollectionViewModel)
        navigationController?.pushViewController(vc, animated: false)
        
        subscribe(to: storiesCollectionViewModel)
    }
    
    private func showStoriesSwiper(with stories: [Story], selectedIndex: Int) {
        
        let vm = AppDelegate.container.resolve(
            StoriesSwiperViewModeling.self,
            arguments: stories.map { StorySwiperCellViewModel($0) } as [StorySwiperCellViewModeling], selectedIndex)!
        
        subscribe(to: vm)
        
        let vc = StoriesSwiperViewController(viewModel: vm)
        
        vc.hero.isEnabled = true
        vc.view.heroID = StoryCollectionCell.heroIdentifier
        vc.heroModalAnimationType = .none
        vc.modalPresentationCapturesStatusBarAppearance = true
        vc.modalPresentationStyle = .custom
        
        navigationController?.present(vc, animated: true, completion: nil)
    }
    
    private func subscribe(to viewModel: StoriesCollectionViewModeling) {
        viewModel.didSelectStoryObservable
            .bind { [weak self] (stories, index) in
                self?.showStoriesSwiper(with: stories, selectedIndex: index)
            }
            .disposed(by: disposeBag)
    }
    
    private func subscribe(to viewModel: StoriesSwiperViewModeling) {
        viewModel.closeSubject
            .take(1)
            .subscribe(onCompleted: { [weak self] in
                if viewModel.currentIndex != viewModel.initialIndex {
                    self?.storiesCollectionViewModel.newlySelectedStoryIndex.accept(viewModel.currentIndex)
                }
                self?.navigationController?.dismiss(animated: true, completion: nil)
            })
            .disposed(by: disposeBag)
    }
}
