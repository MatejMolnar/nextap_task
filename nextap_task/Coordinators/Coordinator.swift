//
//  Coordinator.swift
//  nextap_task
//
//  Created by Matej Molnár on 20/10/2020.
//

import Foundation
import UIKit

protocol Coordinator: AnyObject {
    var navigationController: UINavigationController? { get set }
    var parentCoordinator: Coordinator? { get set }
    var childCoordinators: [Coordinator] { get set }
    
    func start()
    func coordinate(to coordinator: Coordinator)
    func remove(coordinator: Coordinator)
    func removeChildCoordinators()
}

extension Coordinator {
    
    func coordinate(to coordinator: Coordinator) {
        self.childCoordinators += [coordinator]
        coordinator.parentCoordinator = self
        coordinator.start()
    }
    
    func removeChildCoordinators() {
        self.childCoordinators.forEach { $0.removeChildCoordinators() }
        self.childCoordinators.removeAll()
    }
    
    func remove(coordinator: Coordinator){
        if let index = self.childCoordinators.firstIndex(where: { $0 === coordinator }) {
            self.childCoordinators.remove(at: index)
        }
    }
}
