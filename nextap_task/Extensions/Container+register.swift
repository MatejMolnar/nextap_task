//
//  Container+register.swift
//  nextap_task
//
//  Created by Matej Molnár on 20/10/2020.
//

import Swinject

extension Container {
    
    func registerDependencies() {
        registerServices()
        registerCoordinators()
        registerViewModels()
    }
    
    func registerServices() {
        register(Networking.self) { (_) in return HTTPNetworking() }.inObjectScope(.container)
        register(StoriesServicing.self) { (resolver) -> StoriesServicing in
            return StoriesService(networking: resolver.resolve(Networking.self)!)
        }.inObjectScope(.container)
    }
    
    func registerCoordinators() {
        register(StoriesCoordinator.self, factory: {_, navigation in StoriesCoordinator(navigationController: navigation) })
        register(AppCoordinator.self, factory: {_, navigation in AppCoordinator(navigationController: navigation) })
    }
    
    func registerViewModels() {
        register(StoriesCollectionViewModeling.self) { (resolver) -> StoriesCollectionViewModeling in
            return StoriesCollectionViewModel(storiesService: resolver.resolve(StoriesServicing.self)!)
        }
        register(StoriesSwiperViewModeling.self) {
            (resolver: Resolver, modelArray: [StorySwiperCellViewModeling], initialIndex: Int) in
            return StoriesSwiperViewModel(storiesCellModelArray: modelArray, initialIndex: initialIndex)
        }
    }
}
