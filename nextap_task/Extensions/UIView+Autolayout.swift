//
//  UIView+Anchors.swift
//  EventeeSwift
//
//  Created by Matej Molnár on 17/08/2017.
//  Copyright © 2017 Touch Art, s.r.o. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    @discardableResult
    public func sv(_ subViews: UIView?...) -> UIView {
        return sv(subViews)
    }
    
    @discardableResult
    public func sv(_ subViews: [UIView?]) -> UIView {
        for view in subViews.compactMap({ $0 }) {
            addSubview(view)
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        return self
    }
    
    fileprivate func newContraint(_ attribute: NSLayoutConstraint.Attribute,
                                  relatedBy: NSLayoutConstraint.Relation = .equal,
                                  toView: UIView? = nil,
                                  toItemAttribute: NSLayoutConstraint.Attribute? = nil,
                                  multiplier: CGFloat = 1,
                                  constant: CGFloat,
                                  priority: Float = UILayoutPriority.defaultHigh.rawValue + 1) -> UIView {
        
        if toView == nil && (attribute == .width || attribute == .height) {
            let c = constraint(item: self,
                               attribute: attribute,
                               relatedBy: relatedBy,
                               toItem: nil,
                               attribute: nil,
                               multiplier: multiplier,
                               constant: constant,
                               priority: priority)
            addConstraint(c)
            translatesAutoresizingMaskIntoConstraints = false
            
            return self
        }
        
        if let spv = superview {
            let toItem = toView ?? spv
            
            let c = constraint(item: self,
                               attribute: attribute,
                               relatedBy: relatedBy,
                               toItem: toItem,
                               attribute: toItemAttribute,
                               multiplier: multiplier,
                               constant: constant,
                               priority: priority)
            
            if spv.isDescendant(of: toItem) {
                toItem.addConstraint(c)
            } else {
                spv.addConstraint(c)
            }
            
            spv.sv([])
        }
        
        return self
    }
}

fileprivate func constraint(item view1: AnyObject,
                            attribute attr1: NSLayoutConstraint.Attribute,
                            relatedBy: NSLayoutConstraint.Relation = .equal,
                            toItem view2: AnyObject?,
                            attribute attr2: NSLayoutConstraint.Attribute? = nil,
                            multiplier: CGFloat = 1,
                            constant: CGFloat,
                            priority: Float = UILayoutPriority.defaultHigh.rawValue + 1) -> NSLayoutConstraint {
    
    let c =  NSLayoutConstraint(item: view1,
                                attribute: attr1,
                                relatedBy: relatedBy,
                                toItem: view2,
                                attribute: attr2 ?? attr1,
                                multiplier: multiplier,
                                constant: constant)
    
    c.priority = UILayoutPriority(rawValue: priority)
    return c
}

prefix operator >=
@discardableResult
public prefix func >= (p: CGFloat) -> FlexibleMargin {
    return FlexibleMargin(constant: p, relation: .greaterThanOrEqual)
}

prefix operator <=
@discardableResult
public prefix func <= (p: CGFloat) -> FlexibleMargin {
    return FlexibleMargin(constant: p, relation: .lessThanOrEqual)
}

public struct FlexibleMargin {
    var constant: CGFloat
    var relation: NSLayoutConstraint.Relation
}

extension UIView {
    //MARK:- size
    @discardableResult
    func size(_ constant: CGFloat) -> UIView {
        return self.height(constant).width(constant)
    }
    
    //MARK:- height
    @discardableResult
    func height(_ constant: CGFloat) -> UIView {
        return newContraint(.height, constant: constant)
    }
    
    @discardableResult
    func height(_ constant: CGFloat, priority: Float) -> UIView {
        return newContraint(.height, constant: constant, priority: priority)
    }
    
    @discardableResult
    func height(_ margin: FlexibleMargin) -> UIView {
        return newContraint(.height, relatedBy: margin.relation, constant: margin.constant)
    }
    
    @discardableResult
    func height(_ margin: FlexibleMargin, priority: Float) -> UIView {
        return newContraint(.height, relatedBy: margin.relation, constant: margin.constant, priority: priority)
    }
    
    @discardableResult
    func height(superViewMultiplier: CGFloat) -> UIView {
        return newContraint(.height, toView: superview, toItemAttribute: .height, multiplier: superViewMultiplier, constant: 0)
    }
    
    @discardableResult
    func heightEquals(to view: UIView, multiplier: CGFloat = 1) -> UIView {
        return newContraint(.height, toView: view, toItemAttribute: .height, multiplier: multiplier, constant: 0)
    }
    
    @discardableResult
    func heightEqualsWidth(ratio: CGFloat = 1.0) -> UIView {
        return newContraint(.height, toView: self, toItemAttribute: .width, multiplier: ratio, constant: 0)
    }
    
    //MARK:- width
    @discardableResult
    func width(_ constant: CGFloat) -> UIView {
        return newContraint(.width, constant: constant)
    }
    
    @discardableResult
    func width(_ constant: CGFloat, priority: Float) -> UIView {
        return newContraint(.width, constant: constant, priority: priority)
    }
    
    @discardableResult
    func width(_ margin: FlexibleMargin) -> UIView {
        return newContraint(.width, relatedBy: margin.relation, constant: margin.constant)
    }
    
    @discardableResult
    func width(_ margin: FlexibleMargin, priority: Float) -> UIView {
        return newContraint(.width, relatedBy: margin.relation, constant: margin.constant, priority: priority)
    }
    
    @discardableResult
    func width(superViewMultiplier: CGFloat, priority: Float = UILayoutPriority.defaultHigh.rawValue + 1) -> UIView {
        return newContraint(.width, toView: superview, toItemAttribute: .width, multiplier: superViewMultiplier, constant: 0, priority: priority)
    }
    
    @discardableResult
    func widthEquals(to view: UIView, multiplier: CGFloat = 1) -> UIView {
        return newContraint(.width, toView: view, toItemAttribute: .width, multiplier: multiplier, constant: 0)
    }
    
    @discardableResult
    func widthEqualsHeight(ratio: CGFloat = 1.0) -> UIView {
        return newContraint(.width, toView: self, toItemAttribute: .height, multiplier: ratio, constant: 0)
    }
    
    //MARK:- center
    @discardableResult
    func centerY(_ view: UIView? = nil, offset: CGFloat = 0, priority: Float = UILayoutPriority.defaultHigh.rawValue + 1) -> UIView {
        if let spv = superview {
            let toView = view ?? spv
            let c = constraint(item: self, attribute: .centerY, toItem: toView, constant: offset, priority: priority)
            spv.addConstraint(c)
        }
        return self
    }
    
    @discardableResult
    func centerX(_ view: UIView? = nil, offset: CGFloat = 0, priority: Float = UILayoutPriority.defaultHigh.rawValue + 1) -> UIView {
        if let spv = superview {
            let toView = view ?? spv
            let c = constraint(item: self, attribute: .centerX, toItem: toView, constant: offset, priority: priority)
            spv.addConstraint(c)
        }
        return self
    }
    
    @discardableResult
    func centerXY(_ view: UIView? = nil) -> UIView {
        if let spv = superview {
            let toView = view ?? spv
            centerX(toView)
            centerY(toView)
        }
        return self
    }
    
    //MARK:- All Edges
    @discardableResult
    public func followEdges(_ otherView: UIView) -> UIView {
        return newContraint(.top, toView: otherView, toItemAttribute: .top, constant: 0)
            .newContraint(.bottom, toView: otherView, toItemAttribute: .bottom, constant: 0)
            .newContraint(.left, toView: otherView, toItemAttribute: .left, constant: 0)
            .newContraint(.right, toView: otherView, toItemAttribute: .right, constant: 0)
    }
    
    @discardableResult
    func pinToSuperview(inset: CGFloat = 0, priority: Float = UILayoutPriority.defaultHigh.rawValue + 1) -> UIView {
        return self.top(inset, priority: priority).bottom(inset, priority: priority)
            .left(inset, priority: priority).right(inset, priority: priority)
    }
    
    //MARK:- left
    @discardableResult
    func left(_ constant: CGFloat) -> UIView {
        return newContraint(.left, constant: constant)
    }
    
    @discardableResult
    func left(_ margin: FlexibleMargin) -> UIView {
        return newContraint(.left, relatedBy: margin.relation, constant: margin.constant)
    }
    
    @discardableResult
    func left(_ margin: FlexibleMargin, priority: Float) -> UIView {
        return newContraint(.left, relatedBy: margin.relation, constant: margin.constant, priority: priority)
    }
    
    @discardableResult
    func left(_ constant: CGFloat = 0, priority: Float) -> UIView {
        return newContraint(.left, constant: constant, priority: priority)
    }
    
    @discardableResult
    func left(superViewWidthMultiplier: CGFloat) -> UIView {
        let spacer = UIView()
        spacer.isHidden = true
        superview?.sv(spacer)
        spacer.left(0).right(self, .left).height(1).width(superViewMultiplier: superViewWidthMultiplier)
        
        return self
    }
    
    @discardableResult
    func left(_ toView: UIView, _ attribute: NSLayoutConstraint.Attribute, superViewWidthMultiplier: CGFloat) -> UIView {
        let spacer = UIView()
        spacer.isHidden = true
        superview?.sv(spacer)
        
        if superViewWidthMultiplier < 0 {
            spacer.right(toView, attribute).left(self, .left).height(1).width(superViewMultiplier: -superViewWidthMultiplier)
        } else {
            spacer.left(toView, attribute).right(self, .left).height(1).width(superViewMultiplier: superViewWidthMultiplier)
        }
        
        return self
    }
    
    @discardableResult
    func left(_ view: UIView,
              _ attribute: NSLayoutConstraint.Attribute,
              _ constant: CGFloat = 0,
              priority: Float = UILayoutPriority.defaultHigh.rawValue + 1) -> UIView {
        return newContraint(.left, toView: view, toItemAttribute: attribute, constant: constant, priority: priority)
    }
    
    //MARK:- right
    @discardableResult
    func right(_ constant: CGFloat) -> UIView {
        return newContraint(.right, constant: -constant)
    }
    
    @discardableResult
    func right(_ margin: FlexibleMargin) -> UIView {
        return newContraint(.right, relatedBy: margin.relation, constant: -margin.constant)
    }
    
    @discardableResult
    func right(_ margin: FlexibleMargin, priority: Float) -> UIView {
        return newContraint(.right, relatedBy: margin.relation, constant: -margin.constant, priority: priority)
    }
    
    @discardableResult
    func right(_ constant: CGFloat = 0, priority: Float) -> UIView {
        return newContraint(.right, constant: -constant, priority: priority)
    }
    
    @discardableResult
    func right(superViewWidthMultiplier: CGFloat) -> UIView {
        let spacer = UIView()
        spacer.isHidden = true
        superview?.sv(spacer)
        spacer.right(0).left(self, .right).height(1).width(superViewMultiplier: superViewWidthMultiplier)
        
        return self
    }
    
    @discardableResult
    func right(_ toView: UIView, _ attribute: NSLayoutConstraint.Attribute, superViewWidthMultiplier: CGFloat) -> UIView {
        let spacer = UIView()
        spacer.isHidden = true
        superview?.sv(spacer)
        if superViewWidthMultiplier < 0 {
            spacer.left(toView, attribute).right(self, .right).height(1).width(superViewMultiplier: -superViewWidthMultiplier)
        } else {
            spacer.right(toView, attribute).left(self, .right).height(1).width(superViewMultiplier: superViewWidthMultiplier)
        }
        
        return self
    }
    
    @discardableResult
    func right(_ view: UIView,
               _ attribute: NSLayoutConstraint.Attribute,
               _ constant: CGFloat = 0,
               priority: Float = UILayoutPriority.defaultHigh.rawValue + 1) -> UIView {
        return newContraint(.right, toView: view, toItemAttribute: attribute, constant: -constant, priority: priority)
    }
    
    @discardableResult
    func right(_ view: UIView,
               _ attribute: NSLayoutConstraint.Attribute,
               _ margin: FlexibleMargin,
               priority: Float = UILayoutPriority.defaultHigh.rawValue + 1) -> UIView {
        return newContraint(.right, relatedBy: margin.relation, toView: view, toItemAttribute: attribute, constant: -margin.constant, priority: priority)
    }
    
    //MARK:- top
    @discardableResult
    func top(superViewHeightMultiplier: CGFloat) -> UIView {
        let spacer = UIView()
        spacer.isHidden = true
        superview?.sv(spacer)
        spacer.top(0).bottom(self, .top).height(superViewMultiplier: superViewHeightMultiplier).width(1)
        
        return self
    }
    
    @discardableResult
    func top(_ toView: UIView, _ attribute: NSLayoutConstraint.Attribute, superViewHeightMultiplier: CGFloat) -> UIView {
        let spacer = UIView()
        spacer.isHidden = true
        superview?.sv(spacer)
        
        if superViewHeightMultiplier < 0 {
            spacer.bottom(toView, attribute).top(self, .top).height(superViewMultiplier: -superViewHeightMultiplier).width(1)
        } else {
            spacer.top(toView, attribute).bottom(self, .top).height(superViewMultiplier: superViewHeightMultiplier).width(1)
        }
        
        return self
    }
    
    @discardableResult
    func top(_ constant: CGFloat) -> UIView {
        return newContraint(.top, constant: constant)
    }
    
    @discardableResult
    func top(_ margin: FlexibleMargin) -> UIView {
        return newContraint(.top, relatedBy: margin.relation, constant: margin.constant)
    }
    
    @discardableResult
    func top(_ constant: CGFloat = 0, priority: Float) -> UIView {
        return newContraint(.top, constant: constant, priority: priority)
    }
    
    @discardableResult
    func top(_ view: UIView,
             _ attribute: NSLayoutConstraint.Attribute,
             _ constant: CGFloat = 0,
             priority: Float = UILayoutPriority.defaultHigh.rawValue + 1) -> UIView {
        return newContraint(.top, toView: view, toItemAttribute: attribute, constant: constant, priority: priority)
    }
    
    @discardableResult
    func topSafe(_ constant: CGFloat = 0) -> UIView {
        guard let superView = superview else { return self }
        
        if #available(iOS 11.0, *) {
            topAnchor.constraint(equalTo: superView.safeAreaLayoutGuide.topAnchor, constant: constant).isActive = true
        } else {
            topAnchor.constraint(equalTo: superView.topAnchor, constant: constant + SafeAreaHeight.top).isActive = true
        }
        return self
    }
    
    //MARK:- bottom
    @discardableResult
    func bottom(_ constant: CGFloat) -> UIView {
        return newContraint(.bottom, constant: -constant)
    }
    
    @discardableResult
    func bottom(_ margin: FlexibleMargin) -> UIView {
        return newContraint(.bottom, relatedBy: margin.relation, constant: margin.constant)
    }
    
    @discardableResult
    func bottom(_ constant: CGFloat = 0, priority: Float) -> UIView {
        return newContraint(.bottom, constant: -constant, priority: priority)
    }
    
    @discardableResult
    func bottom(_ view: UIView,
                _ attribute: NSLayoutConstraint.Attribute,
                _ constant: CGFloat = 0,
                priority: Float = UILayoutPriority.defaultHigh.rawValue + 1) -> UIView {
        return newContraint(.bottom, toView: view, toItemAttribute: attribute, constant: -constant, priority: priority)
    }
    
    @discardableResult
    func bottom(superViewHeightMultiplier: CGFloat) -> UIView {
        let spacer = UIView()
        spacer.isHidden = true
        superview?.sv(spacer)
        spacer.bottom(0).top(self, .bottom).height(superViewMultiplier: superViewHeightMultiplier).width(1)
        
        return self
    }
    
    @discardableResult
    func bottom(_ toView: UIView, _ attribute: NSLayoutConstraint.Attribute, superViewHeightMultiplier: CGFloat) -> UIView {
        let spacer = UIView()
        spacer.isHidden = true
        superview?.sv(spacer)
        
        if superViewHeightMultiplier < 0 {
            spacer.top(toView, attribute).bottom(self, .bottom).height(superViewMultiplier: -superViewHeightMultiplier).width(1)
        } else {
            spacer.bottom(toView, attribute).top(self, .bottom).height(superViewMultiplier: superViewHeightMultiplier).width(1)
        }
        
        return self
    }
    
    @discardableResult
    func bottomSafe(_ constant: CGFloat = 0) -> UIView {
        guard let superView = superview else { return self }
        
        if #available(iOS 11.0, *) {
            bottomAnchor.constraint(equalTo: superView.safeAreaLayoutGuide.bottomAnchor, constant: -constant).isActive = true
        } else {
            bottomAnchor.constraint(equalTo: superView.bottomAnchor, constant: -(constant + SafeAreaHeight.bottom)).isActive = true
        }
        return self
    }
}

//MARK:- Getters
fileprivate func constraintForView(_ view: UIView, attribute: NSLayoutConstraint.Attribute) -> NSLayoutConstraint? {
    
    var target = view.superview ?? view
    
    if attribute == .height || attribute == .width {
        target = view
    }
    
    for c in target.constraints {
        if let fi = c.firstItem as? NSObject, fi == view && c.firstAttribute == attribute {
            return c
        }
        if let si = c.secondItem as? NSObject, si == view && c.secondAttribute == attribute {
            return c
        }
    }
    
    return nil
}

struct SafeAreaHeight {
    static var top: CGFloat {
        if #available(iOS 11.0, *) {
            if let top = UIApplication.shared.windows.filter({$0.isKeyWindow}).first?.safeAreaInsets.top {
                return top == 0 ? 20 : top
            }
        }
        
        return 20
    }
    
    static var bottom: CGFloat {
        if #available(iOS 11.0, *) {
            if let window = UIApplication.shared.windows.filter({$0.isKeyWindow}).first {
                return window.safeAreaInsets.bottom
            }
        }
        
        return 0
    }
}

extension UIView {
    public var centerYConstraint: NSLayoutConstraint? {
        get {
            return associatedConstraint(.centerY, key: &Keys.centerY)
        }
    }
    
    public var centerXConstraint: NSLayoutConstraint? {
        get {
            return associatedConstraint(.centerX, key: &Keys.centerX)
        }
    }
    
    public var leftConstraint: NSLayoutConstraint? {
        get {
            return associatedConstraint(.left, key: &Keys.left)
        }
    }
    
    public var rightConstraint: NSLayoutConstraint? {
        get {
            return associatedConstraint(.right, key: &Keys.right)
        }
    }
    
    public var topConstraint: NSLayoutConstraint? {
        get {
            return associatedConstraint(.top, key: &Keys.top)
        }
    }
    
    public var bottomConstraint: NSLayoutConstraint? {
        get {
            return associatedConstraint(.bottom, key: &Keys.bottom)
        }
    }
    
    public var heightConstraint: NSLayoutConstraint? {
        get {
            return associatedConstraint(.height, key: &Keys.height)
        }
    }
    
    public var widthConstraint: NSLayoutConstraint? {
        get {
            return associatedConstraint(.width, key: &Keys.width)
        }
    }
    
    func associatedConstraint(_ attribute: NSLayoutConstraint.Attribute, key: UnsafeRawPointer) -> NSLayoutConstraint? {
        var constraint: NSLayoutConstraint?
        
        if let contr = objc_getAssociatedObject(self, key) as? NSLayoutConstraint {
            constraint = contr
        }
        else {
            constraint = constraintForView(self, attribute: attribute)
            objc_setAssociatedObject(self, key, constraint, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
        
        return constraint
    }
}

fileprivate struct Keys {
    static var top = "AssociatedConstraintTop"
    static var bottom = "AssociatedConstraintBottom"
    static var left = "AssociatedConstraintLeft"
    static var right = "AssociatedConstraintRight"
    static var width = "AssociatedConstraintWidth"
    static var height = "AssociatedConstraintHeight"
    static var centerX = "AssociatedConstraintCenterX"
    static var centerY = "AssociatedConstraintCenterY"
}
