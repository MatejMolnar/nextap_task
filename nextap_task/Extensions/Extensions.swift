//
//  Extensions.swift
//  nextap_task
//
//  Created by Matej Molnár on 17/10/2020.
//

import Foundation

extension Array {
    
    func safeObject(_ index: Int) -> Element? {
        guard index < self.count, index >= 0 else { return nil }
        return self[index]
    }
}
