//
//  UIExtensions.swift
//  nextap_task
//
//  Created by Matej Molnár on 17/10/2020.
//

import UIKit

extension UICollectionView {
    func registerCellClasses(_ array: [UICollectionViewCell.Type]) {
        for type in array {
            registerCellClass(ofType: type)
        }
    }
    
    func registerCellClass<T: UICollectionViewCell>(ofType type: T.Type) {
        register(type, forCellWithReuseIdentifier: String(describing: type))
    }
    
    func dequeReusableCell<T: UICollectionViewCell>(ofType type: T.Type, for indexPath: IndexPath) -> T {
        if let cell = dequeueReusableCell(withReuseIdentifier: String(describing: type), for: indexPath) as? T {
            return cell
        }
        
        fatalError("Trying to deque cell of wrong type")
    }
}
