//
//  Endpoint.swift
//  nextap_task
//
//  Created by Matej Molnár on 17/10/2020.
//

import Foundation

protocol Endpoint {
    /// The target's base `URL`.
    var baseURL: String { get }
    
    /// The path to be appended to `baseURL` to form the full `URL`.
    var path: String { get }
}

extension Endpoint {
    var fullURL: String {
        return baseURL + path
    }
}
