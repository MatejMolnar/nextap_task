//
//  HTTPNetworking.swift
//  nextap_task
//
//  Created by Matej Molnár on 19/10/2020.
//

import RxSwift

struct HTTPNetworking: Networking {
  
    func dataRequestSingle(with endpoint: Endpoint) -> Single<Data?> {
        return Single<Data?>.create { closure in
            guard let url = URL(string: endpoint.fullURL) else {
                closure(.error(NetworkingError.invalidUrl))
                return Disposables.create()
            }
            
            let request = createRequest(from: url)
            let task = URLSession.shared.dataTask(with: request) { data, httpResponse, error in
                if let error = error {
                    closure(.error(error))
                } else {
                    closure(.success(data))
                }
            }
            
            task.resume()
            
            return Disposables.create {
                task.cancel()
            }
        }
    }
    
    func dataRequestSingle<T: Codable>(with endpoint: Endpoint, responseType: T.Type) -> Single<T> {
        return dataRequestSingle(with: endpoint).map { data -> T in
            guard let data = data else {
                throw NetworkingError.noResponseData
            }
                        
            let codableResponse = try JSONDecoder().decode(responseType, from: data)
            return codableResponse
        }
    }
    
    private func createRequest(from url: URL) -> URLRequest {
        var request = URLRequest(url: url)
        request.cachePolicy = .reloadIgnoringCacheData
        return request
    }
}
