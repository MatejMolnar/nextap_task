//
//  Networking.swift
//  nextap_task
//
//  Created by Matej Molnár on 17/10/2020.
//

import RxSwift

protocol Networking {
    /// Returns Single<Response> that makes a request each time its subscribed to.
    func dataRequestSingle(with endpoint: Endpoint) -> Single<Data?>
    
    func dataRequestSingle<T: Codable>(with endpoint: Endpoint, responseType: T.Type) -> Single<T>
}

