//
//  NetworkingError.swift
//  nextap_task
//
//  Created by Matej Molnár on 17/10/2020.
//

import Foundation

enum NetworkingError: Error {
    case invalidResponseType
    case invalidUrl
    case noResponseData
    case jsonCouldNotBeParsedToObject    
}
