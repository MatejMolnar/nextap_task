//
//  StellerEndpoint.swift
//  nextap_task
//
//  Created by Matej Molnár on 17/10/2020.
//

import Foundation

enum StellerEndpoint {
    case getStories
}

extension StellerEndpoint: Endpoint {
    var baseURL: String {
        return "https://api.steller.co/v1"
    }
    
    var path: String {
        switch self {
        case .getStories: return "/users/76794126980351029/stories"
        }
    }
}
