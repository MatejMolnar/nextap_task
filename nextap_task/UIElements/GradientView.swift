//
//  GradientVIew.swift
//  nextap_task
//
//  Created by Matej Molnár on 18/10/2020.
//

import UIKit

class GradientView: UIView {
    
    var startColor: UIColor = .black { didSet { updateColors() } }
    var endColor: UIColor = .white { didSet { updateColors() } }
    var startLocation: Double = 0.05 { didSet { updateLocations() } }
    var endLocation: Double = 0.95 { didSet { updateLocations() } }
    var horizontalMode: Bool = false { didSet { updatePoints() } }
    var diagonalMode: Bool = false { didSet { updatePoints() } }
    
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
    
    var gradientLayer: CAGradientLayer {
        return layer as! CAGradientLayer
    }
    
    private func updatePoints() {
        if horizontalMode {
            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 1, y: 0) : CGPoint(x: 0, y: 0.5)
            gradientLayer.endPoint = diagonalMode ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0.5)
        } else {
            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 0, y: 0) : CGPoint(x: 0.5, y: 0)
            gradientLayer.endPoint = diagonalMode ? CGPoint(x: 1, y: 1) : CGPoint(x: 0.5, y: 1)
        }
    }
    
    private func updateLocations() {
        gradientLayer.locations = [startLocation as NSNumber, endLocation as NSNumber]
    }
    
    private func updateColors() {
        gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updatePoints()
        updateLocations()
        updateColors()
    }
}
