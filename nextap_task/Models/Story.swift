//
//  Story.swift
//  nextap_task
//
//  Created by Matej Molnár on 17/10/2020.
//

import Foundation

struct Story: Codable {
    
    struct UserInfo: Codable {
        enum CodingKeys: String, CodingKey {
            case name = "display_name"
            case avatarImageUrl = "avatar_image_url"
        }
        
        let name: String
        let avatarImageUrl: String
    }
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case coverImageUrl = "cover_src"
        case userInfo = "user"
    }
    
    let id: String
    let coverImageUrl: String
    private let userInfo: UserInfo
    
    var userName: String {
        return userInfo.name
    }
    var userImageUrl: String {
        return userInfo.avatarImageUrl
    }
}


