//
//  StoriesService.swift
//  nextap_task
//
//  Created by Matej Molnár on 17/10/2020.
//

import RxSwift

class StoriesService: StoriesServicing {
    
    struct FetchStoriesResponse: Codable {
        enum CodingKeys: String, CodingKey {
            case stories = "data"
        }
        
        let stories: [Story]        
    }
    
    private let networking: Networking
    
    init(networking: Networking) {
        self.networking = networking
    }
    
    func fetchStoriesSingle() -> Single<[Story]> {
        return networking
            .dataRequestSingle(with: StellerEndpoint.getStories, responseType: FetchStoriesResponse.self)
            .map { $0.stories }
    }
}


