//
//  StoriesServicing.swift
//  nextap_task
//
//  Created by Matej Molnár on 17/10/2020.
//

import RxSwift

protocol StoriesServicing {
    func fetchStoriesSingle() -> Single<[Story]>
}
