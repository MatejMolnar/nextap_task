//
//  nextap_taskTests.swift
//  nextap_taskTests
//
//  Created by Matej Molnár on 17/10/2020.
//

import XCTest
import RxSwift
import Swinject
import RxBlocking

@testable import nextap_task

enum Dataset: CaseIterable {
    case correct, corrupted, noData
    
    var fileName: String {
        switch self {
        case .correct: return "stories_dataset_correct"
        case .corrupted: return "stories_dataset_corrupted"
        case .noData: return "nonExistingFileName"
        }
    }
}

class StoriesServiceTests: XCTestCase {
    
    private let container = Container()
    
    override func setUpWithError() throws {
        container.register(Networking.self) { _, fileName in NetworkingMock(fileName: fileName) }
        
        for dataset in Dataset.allCases {
            container.register(StoriesServicing.self, name: dataset.fileName) { resolver -> StoriesServicing in
                return StoriesService(networking: resolver.resolve(Networking.self, argument: dataset.fileName)!)
            }
        }
    }

    override func tearDownWithError() throws {
        container.removeAll()
    }

    func testFetchStoriesWithDataset_correct() throws {
        let service = container.resolve(StoriesServicing.self, name: Dataset.correct.fileName)!
                
        XCTAssertEqual(try service.fetchStoriesSingle().toBlocking().first()?.count, 1)
    }

    func testFetchStoriesWithDataset_corrupted() throws {
        let service = container.resolve(StoriesServicing.self, name: Dataset.corrupted.fileName)!
                
        XCTAssertThrowsError(try service.fetchStoriesSingle().toBlocking().first()) { error in
            XCTAssertEqual(error as! NetworkingError, NetworkingError.jsonCouldNotBeParsedToObject)
        }
    }
    
    func testFetchStoriesWithDataset_noData() throws {
        let service = container.resolve(StoriesServicing.self, name: Dataset.noData.fileName)!
                
        XCTAssertThrowsError(try service.fetchStoriesSingle().toBlocking().first()) { error in
            XCTAssertEqual(error as! NetworkingError, NetworkingError.noResponseData)
        }
    }
}
