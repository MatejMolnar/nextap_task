//
//  NetworkingMock.swift
//  nextap_taskTests
//
//  Created by Matej Molnár on 20/10/2020.
//

import XCTest
import RxSwift

@testable import nextap_task

struct NetworkingMock: Networking {
    let fileName: String
    
    func dataRequestSingle(with endpoint: Endpoint) -> Single<Data?> {
        
        return Single<Data?>.create { closure in
            closure(.success(readJSON(name: fileName)))
            return Disposables.create { }
        }
    }
    
    func dataRequestSingle<T>(with endpoint: Endpoint, responseType: T.Type) -> Single<T> where T : Decodable, T : Encodable {
        return dataRequestSingle(with: endpoint).map { data -> T in
            guard let data = data else {
                throw NetworkingError.noResponseData
            }
                        
            do {
                let codableResponse = try JSONDecoder().decode(responseType, from: data)
                return codableResponse
            } catch {
                throw NetworkingError.jsonCouldNotBeParsedToObject
            }
        }
    }
    
    private func readJSON(name: String) -> Data? {
        let bundle = Bundle(for: StoriesServiceTests.self)
        
      guard let url = bundle.url(forResource: name, withExtension: "json") else { return nil }
      
      do {
        return try Data(contentsOf: url, options: .mappedIfSafe)
      }
      catch {
        XCTFail("Error occurred parsing test data")
        return nil
      }
    }
}
