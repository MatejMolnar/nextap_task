Architecture: MVVMC  
Binding framework: [RxSwift + RxCocoa](https://github.com/ReactiveX/RxSwift)  
Dependency injection: [Swinject](https://github.com/Swinject/Swinject)  
UI layout: Custom lightweight library (Extensions/UIView+Autolayout) inspired by [Stevia](https://github.com/freshOS/Stevia)  
ViewController transitions: [Hero](https://github.com/HeroTransitions/Hero)  
Image downloading + caching: [Kingfisher](https://github.com/onevcat/Kingfisher)  
Swiper 3D animation: [FSPagerView](https://github.com/WenchaoD/FSPagerView)